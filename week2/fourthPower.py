from square import *

# a function that takes in one number and returns that value raised to the fourth power.


def fourthPower(x):
    '''
    x: int or float.
    '''
    # Your code here
    return square(square(x))

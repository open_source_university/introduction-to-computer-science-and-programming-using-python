from math import tan, pi

# A function to calculate the sum of area and square of perimeter for a regular polygon


def polysum(n, s):
    """
    Input: n, a positive int; s, a positive number
    Returns: a positive number
    """
    area = n * (s ** 2) / (4 * tan(pi/n))
    perimeter = n * s
    return round(area + perimeter ** 2, 4)

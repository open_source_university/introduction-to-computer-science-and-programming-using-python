balance = 42
annualInterestRate = 0.2
monthlyPaymentRate = 0.04

def banksSuck(balance, AIR, MPR):
  unpaidBalance = balance
  for i in range(12):
    monthlyPayment = unpaidBalance * MPR
    unpaidBalance -= monthlyPayment
    unpaidBalance += unpaidBalance * AIR/12
    print('Month ' + str(i + 1) + ' Remaining balance: ' + str(round(unpaidBalance, 2)))
  return round(unpaidBalance, 2)
  

print('Remaining balance: ' + str(banksSuck(484, 0.2, 0.04)))

# An iterative function iterPower(base, exp) that calculates the exponential baseexp by simply using successive multiplication.
def iterPower(base, exp):
    '''
    base: int or float.
    exp: int >= 0

    returns: int or float, base^exp
    '''
    # Your code here
    result = base
    if exp == 0:
        return 1
    for _ in range(1, exp):
        result *= base
    return result

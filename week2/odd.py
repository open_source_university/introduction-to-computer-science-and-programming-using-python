# A function that takes in one number and returns True when the number is odd and False otherwise.
def odd(x):
    '''
    x: int

    returns: True if x is odd, False otherwise
    '''
    # Your code here
    return x % 2 == 1

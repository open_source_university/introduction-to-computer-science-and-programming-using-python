# This time, write a procedure, called biggest, which returns the key corresponding to the entry with the largest number of values associated with it. If there is more than one such entry, return any one of the matching keys.
# Example usage:

# >>> biggest(animals)
# 'd'


def biggest(aDict):
    '''
    aDict: A dictionary, where all the values are lists.

    returns: The key with the largest number of values associated with it
    '''
    # Your Code Here
    biggest = 0
    for key in aDict:
        if len(aDict[key]) > biggest:
            biggest = key
    return biggest

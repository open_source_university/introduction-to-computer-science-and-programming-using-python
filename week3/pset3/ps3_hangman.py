import re

# Hangman game
#

# -----------------------------------
# Helper code
# You don't need to understand this helper code,
# but you will have to know how to use the functions
# (so be sure to read the docstrings!)

import random

WORDLIST_FILENAME = "/home/momchil.kolev@scalefocus.com/Edx/Mitx/week3/pset3/words.txt"


def loadWords():
    """
    Returns a list of valid words. Words are strings of lowercase letters.

    Depending on the size of the word list, this function may
    take a while to finish.
    """
    print("Loading word list from file...")
    # inFile: file
    inFile = open(WORDLIST_FILENAME, 'r')
    # line: string
    line = inFile.readline()
    # wordlist: list of strings
    wordlist = line.split()
    print("  ", len(wordlist), "words loaded.")
    return wordlist


def chooseWord(wordlist):
    """
    wordlist (list): list of words (strings)

    Returns a word from wordlist at random
    """
    return random.choice(wordlist)

# end of helper code
# -----------------------------------


# Load the list of words into the variable wordlist
# so that it can be accessed from anywhere in the program
wordlist = loadWords()


def isWordGuessed(secretWord, lettersGuessed):
    '''
    secretWord: string, the word the user is guessing
    lettersGuessed: list, what letters have been guessed so far
    returns: boolean, True if all the letters of secretWord are in lettersGuessed;
      False otherwise
    '''
    # FILL IN YOUR CODE HERE...


def getGuessedWord(secretWord, lettersGuessed):
    '''
    secretWord: string, the word the user is guessing
    lettersGuessed: list, what letters have been guessed so far
    returns: string, comprised of letters and underscores that represents
      what letters in secretWord have been guessed so far.
    '''
    # FILL IN YOUR CODE HERE...


def getAvailableLetters(lettersGuessed):
    '''
    lettersGuessed: list, what letters have been guessed so far
    returns: string, comprised of letters that represents what letters have not
      yet been guessed.
    '''
    # FILL IN YOUR CODE HERE...


def hangman(secretWord):
    '''
    secretWord: string, the secret word to guess.

    Starts up an interactive game of Hangman.

    * At the start of the game, let the user know how many 
      letters the secretWord contains.

    * Ask the user to supply one guess (i.e. letter) per round.

    * The user should receive feedback immediately after each guess 
      about whether their guess appears in the computers word.

    * After each round, you should also display to the user the 
      partially guessed word so far, as well as letters that the 
      user has not yet guessed.

    Follows the other limitations detailed in the problem write-up.
    '''
    # FILL IN YOUR CODE HERE...
    # print('The secret word is ', len(secretWord),
    # ' letters and is ', secretWord)
    guesses = 8
    letters = 'abcdefghijklmnopqrstuvwxyz'
    usedLetters = ''
    won = False
    print('Welcome to the game, Hangman!')
    print('I am thinking of a word that is ' +
          str(len(secretWord)) + ' letters long.')

    def formatGuessed(secretWord, letters):
        word = ''
        for c in secretWord:
            word += '_' if c in letters else c
            # print('word is', word)
        return word

    def round(letters, guesses, won, usedLetters):
        print('- - - - - - - - - -')
        print('You have ' + str(guesses) + ' guesses left.')
        print('Available letters: ' + letters)
        # print('Unavailable letters: ' + usedLetters)
        guess = input('Please guess a letter: ').tolower()
        # Let the user know he's already used that letter
        if guess not in letters:
            print("You've already used the letter " + guess)
            return (letters, guesses, won, usedLetters)
        usedLetters += guess
        letters = re.sub(guess, '', letters)
        if guess in secretWord:
            # create currently guessed word
            word = formatGuessed(secretWord, letters)
            print('Good guess: ' + word)
            if word.count('_') == 0:
                won = True
        else:
            word = formatGuessed(secretWord, letters)
            print('Oops! That letter is not in my word: ' + word)
            guesses -= 1
        return (letters, guesses, won, usedLetters)

    while guesses > 0:
        (letters, guesses, won, usedLetters) = round(
            letters, guesses, won, usedLetters)
        if won == True:
            print('Congratulations, you won!')
            return
    print('Sorry, you ran out of guesses. The word was ' + secretWord + '.')


# When you've completed your hangman function, uncomment these two lines
# and run this file to test! (hint: you might want to pick your own
# secretWord while you're testing)


secretWord = chooseWord(wordlist).lower()
hangman(secretWord)

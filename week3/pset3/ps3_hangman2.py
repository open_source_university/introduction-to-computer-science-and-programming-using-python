from getAvailableLetters import *
from isWordGuessed import *
from getGuessedWord import *


def hangman(secretWord):
    '''
    secretWord: string, the secret word to guess.

    Starts up an interactive game of Hangman.

    * At the start of the game, let the user know how many 
      letters the secretWord contains.

    * Ask the user to supply one guess (i.e. letter) per round.

    * The user should receive feedback immediately after each guess 
      about whether their guess appears in the computers word.

    * After each round, you should also display to the user the 
      partially guessed word so far, as well as letters that the 
      user has not yet guessed.

    Follows the other limitations detailed in the problem write-up.
    '''
    # FILL IN YOUR CODE HERE...
    lettersGuessed = ''
    mistakesMade = 0
    availableLetters = 'abcdefghijklmnopqrstuvwxyz'
    won = False

    print('Welcome to the game, Hangman!')
    print('I am thinking of a word that is ' +
          str(len(secretWord)) + ' letters long.')

    def round(availableLetters, mistakesMade, lettersGuessed, won):
        print('- - - - - - - - - -')
        print('You have ' + str(8 - mistakesMade) + ' guesses left.')
        print('Available letters: ' + availableLetters)
        guess = input('Please guess a letter: ').lower()

        if guess not in availableLetters:
            word = getGuessedWord(secretWord, lettersGuessed)
            print("Oops! You've already guessed that letter: " + word)
            return (availableLetters, mistakesMade, lettersGuessed, False)

        lettersGuessed += guess
        availableLetters = getAvailableLetters(lettersGuessed)

        if guess in secretWord:
            word = getGuessedWord(secretWord, lettersGuessed)
            print('Good guess: ' + word)
            if word.count('_') == 0:
                print('Congratulations, you won!')
                return (availableLetters, mistakesMade, lettersGuessed, True)
        else:
            word = getGuessedWord(secretWord, lettersGuessed)
            print('Oops! That letter is not in my word: ' + word)
            mistakesMade += 1
        return (availableLetters, mistakesMade, lettersGuessed, False)

    while mistakesMade < 8 and not won:
        (availableLetters, mistakesMade, lettersGuessed, won) = round(
            availableLetters, mistakesMade, lettersGuessed, won)
        if mistakesMade == 8:
            print('Sorry, you ran out of guesses. The word was ' + secretWord + '.')
            return 'Sorry, you ran out of guesses. The word was ' + secretWord + '.'

    return 'Congratulations, you won!'


hangman('c')

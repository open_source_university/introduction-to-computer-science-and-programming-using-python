def while3(end):
    n = 1
    res = 0
    while n < end:
        res += n
        n += 1
    print(res + end)

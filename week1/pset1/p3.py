# Write a program that prints the longest substring of s in which the letters occur in alphabetical order. For example, if s = 'azcbobobegghakl', then your program should print
# Longest substring in alphabetical order is: beggh


def longestSubStr(s):
    longest = ''
    current = ''
    for char in s:
        if len(current) == 0 or char >= current[-1]:
            current += char
            if len(current) > len(longest):
                longest = current
        elif char <= current[-1]:
            if len(current) > len(longest):
                longest = current
                current = char
    print('Longest substring in alphabetical order is: ' + longest)

# Write a program that prints the number of times the string 'bob' occurs in s. For example, if s = 'azcbobobegghakl', then your program should print
def bobs(s):
    count = 0
    for n in range(0, len(s)):
        # n:n+3 to check each 3-letter combination, 3-letter because bob is 3 letters
        if s[n:n+3] == 'bob':
            count += 1
    print('Number of times bob occurs is: ' + str(count))
